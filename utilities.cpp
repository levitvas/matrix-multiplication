//
// Created by Vasily on 12/23/2023.
//
#include "utilities.h"

std::string matrix_size(size_t m1, size_t n1) {
    return "(" + std::to_string(m1) + ", " + std::to_string(n1) + ")";
}

void print_matrix(std::ostream &out, const std::vector<double> &matrix,
                  size_t m, size_t n, const std::string &name) {
    out << "Matrix " << name << ": " + matrix_size(m, n) << std::endl;
    for(size_t row = 0; row < m; row++){
        for(size_t column = 0; column < n; column++)
            out << matrix[row * n + column] << " ";
        out << std::endl;
    }
    out << std::endl;
}

void generate_print_matrix(std::ostream &out, const std::vector<double> &matrix,
                           size_t m, size_t n, const std::string &name) {
    out << m << " " << std::to_string(n) << std::endl;
    for(size_t row = 0; row < m; row++){
        for(size_t column = 0; column < n; column++)
            out << matrix[row * n + column] << " ";
        out << std::endl;
    }
}

void exit_with_error(std::ostream &out, int code, const std::string &error_message) {
    out << error_message << std::endl;
    exit(code);
}

bool check_matrix_size(size_t row, size_t col) {
    return (row < 1 || col < 1);
}

Matrix get_matrix_input(std::istream &stream, size_t &row, size_t &col) {
    stream >> row >> col;
    if (!stream) exit_with_error(std::cerr, INCORRECT_INPUT_C, INCORRECT_INPUT_MESSAGE);
    if(check_matrix_size(row, col)) exit_with_error(std::cerr, WRONG_SIZE_C, WRONG_SIZE_MESSAGE);
    Matrix matrix(row * col);
    for(size_t i = 0; i < row; i++)
        for(size_t j = 0; j < col; j++){
            stream >> matrix[i * col + j];
            if(!stream) exit_with_error(std::cerr, INCORRECT_INPUT_C, INCORRECT_INPUT_MESSAGE);
        }

    return  matrix;
}
