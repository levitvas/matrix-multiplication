//
// Created by Vasily on 12/23/2023.
//

#ifndef MATRIX_MULT_SEMERSTRAL_UTILITIES_H
#define MATRIX_MULT_SEMERSTRAL_UTILITIES_H

#include <string>
#include <vector>
#include <iostream>

using Matrix = std::vector<double>;

#define HELP_MESSAGE "-p Will enable multithreading.\n\
-f <path> Matrices will be read from a file.\n\
-g Will generate random matrices with random sizes with predifined boundaries.\n\
-o Will output matrices to std::cout.\n\
-of <path> Will output to the specified file.\n\
-ofg <path> Will output the generated matrices to the file with format suitable for input."

#define INVALID_ARGUMENT_MESSAGE "--+-+-+ERROR: Invalid argument+-+-+--"
#define INCORRECT_INPUT_MESSAGE "--+-+-+ERROR: Incorrect input+-+-+--"
#define NOT_FOUND_MESSAGE "--+-+-+ERROR: Recheck provided file path+-+-+--"
#define WRONG_SIZE_MESSAGE "--+-+-+ERROR: Incorrect matrix sizes+-+-+--"

const int MIN_MATRIX_DOUBLE = 1;
const int MAX_MATRIX_DOUBLE = 3;
const int MIN_MATRIX_SIZE = 10;
const int MAX_MATRIX_SIZE = 10;

enum {
    INVALID_ARGUMENT_C = 123,
    INCORRECT_INPUT_C = 4,
    NOT_FOUND_C = 5,
    WRONG_SIZE_C = 6
};

std::string matrix_size(size_t m1, size_t n1);
void print_matrix(std::ostream &out, const std::vector<double> &matrix,
                  size_t m, size_t n, const std::string &name);
void generate_print_matrix(std::ostream &out, const std::vector<double> &matrix,
                           size_t m, size_t n, const std::string &name);
void exit_with_error(std::ostream &out, int code, const std::string &error_message);
bool check_matrix_size(size_t row, size_t col);
Matrix get_matrix_input(std::istream &stream, size_t &row, size_t &col);

#endif //MATRIX_MULT_SEMERSTRAL_UTILITIES_H