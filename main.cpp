#include <iostream>
#include <chrono>
#include <random>
#include <filesystem>
#include <algorithm>
#include <string>
#include <fstream>
#include <thread>
#include "utilities.h"

template <typename TimePoint> std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

double get_random_double(int a, int b){
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(a, b);
    return dist(mt);
}

size_t get_random_size(int a, int b){
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_int_distribution<> dist(a, b);
    return dist(mt);
}

Matrix transpose_matrix(const Matrix& M, size_t m, size_t n) {
    Matrix T(m * n);
    for (size_t i = 0; i < m; ++i)
        for (size_t j = 0; j < n; ++j)
            T[j * m + i] = M[i * n + j];

    return T;
}

Matrix multiply_matrices(const Matrix &A, const Matrix &B, size_t m1, size_t n1, size_t m2, size_t n2) {
    Matrix B_transposed = transpose_matrix(B, m2, n2);
    Matrix result(m1 * n2, 0);

    for (size_t i = 0; i < m1; ++i)
        for (size_t j = 0; j < n2; ++j)
            for (size_t k = 0; k < n1; ++k)
                result[i * n2 + j] += A[i * n1 + k] * B_transposed[j * m2 + k];

    return result;
}

void multiply_section(const Matrix &A, const Matrix &B, Matrix &result, size_t start_row, size_t end_row, size_t n1, size_t n2, size_t m2) {
    for (size_t i = start_row; i < end_row; ++i) {
        for (size_t j = 0; j < n2; ++j) {
            double sum = 0;
            for (size_t k = 0; k < n1; ++k) {
                sum += A[i * n1 + k] * B[j * m2 + k];
            }
            result[i * n2 + j] = sum;
        }
    }
}

Matrix multiply_matrices_parallel(const Matrix &A, const Matrix &B, size_t m1, size_t n1, size_t m2, size_t n2) {
    Matrix result(m1 * n2, 0.0);
    Matrix B_transposed = transpose_matrix(B, m2, n2);
    size_t num_threads = std::thread::hardware_concurrency();
    std::vector<std::thread> threads(num_threads);

    size_t rows_per_thread = m1 / num_threads;
    size_t start_row = 0;
    for (size_t i = 0; i < num_threads; ++i) {
        size_t end_row = (i == num_threads - 1) ? m1 : start_row + rows_per_thread;
        threads[i] = std::thread(multiply_section, std::cref(A), std::cref(B_transposed), std::ref(result), start_row, end_row, n1, n2, m2);
        start_row = end_row;
    }

    for (auto& thread : threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }

    return result;
}

Matrix create_random_matrix(size_t row, size_t col) {
    Matrix matrix(row * col);
    for(size_t i = 0; i < row * col; i++)
        matrix[i] = get_random_double(MIN_MATRIX_DOUBLE, MAX_MATRIX_DOUBLE);

    return matrix;
}

void thread_create_random_matrix(Matrix &matrix, size_t begin, size_t end) {
    for (size_t i = begin; i < end; i++) {
        matrix[i] = get_random_double(MIN_MATRIX_DOUBLE, MAX_MATRIX_DOUBLE);
    }
}

void generate_matrices(Matrix &A, Matrix &B, size_t m1, size_t n1, size_t m2, size_t n2) {
    A = create_random_matrix(m1, n1);
    B = create_random_matrix(m2, n2);
}

void generate_matrices_parallel(Matrix &A, Matrix &B) {
    size_t num_threads = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    // lambda function
    auto create_threads = [&](Matrix &matrix) {
        size_t size = matrix.size();
        size_t chunk_size = 1 + size / num_threads;

        for (size_t thread_id = 0; thread_id < num_threads; ++thread_id) {
            size_t begin = thread_id * chunk_size;
            size_t end = std::min((thread_id + 1) * chunk_size, size);

            threads.emplace_back(thread_create_random_matrix, std::ref(matrix), begin, end);
        }
    };

    create_threads(A);
    create_threads(B);

    for (auto &t : threads) {
        if (t.joinable()) {
            t.join();
        }
    }
}

int main(int argc, char** argv) {
    auto start = std::chrono::high_resolution_clock::now();
    int matrix_input = 0;
    bool parallel = false;
    bool print_matrix_out = false;
    bool generate_matrix_out = false;
    std::string matrix_path;
    std::string matrix_output_path;
    std::string generate_output_path;
    std::ifstream file_stream;
    size_t m1 = 0, m2 = 0, n1 = 0, n2 = 0;
    Matrix A, B, C;

    for(int i = 1; i < argc; i++){
        std::string arg = argv[i];
        if(arg == "--help"){
            std::cout << HELP_MESSAGE << std::endl;
            return 0;
        } else if(arg == "-f"){
            matrix_input = 1;
            if (i + 1 >= argc) exit_with_error(std::cerr, INVALID_ARGUMENT_C, INVALID_ARGUMENT_MESSAGE);
            matrix_path = argv[++i];
            if (!std::filesystem::exists(matrix_path)) {
                exit_with_error(std::cerr, NOT_FOUND_C, NOT_FOUND_MESSAGE);
            }
            std::cout << "-=-=Matrices from the following file: " << matrix_path << "=-=-" << std::endl;
        } else if(arg == "-g"){
            matrix_input = 2;
            std::cout << "-=-=Matrices with random sizes will be automatically generated=-=-" << std::endl;
        } else if(arg == "-p"){
            parallel = true;
            std::cout << "-=-=Parallel mode enabled=-=-" << std::endl;
        } else if(arg == "-o"){
            print_matrix_out = true;
            std::cout << "-=-=Matrices will be written to std::cout=-=-" << std::endl;
        } else if (arg == "-of") {
            print_matrix_out = true;
            if (i + 1 >= argc) exit_with_error(std::cerr, INVALID_ARGUMENT_C, INVALID_ARGUMENT_MESSAGE);
            matrix_output_path = argv[++i];
            std::cout << "-=-=Matrices will be written to the following file: " << matrix_output_path << "=-=-" << std::endl;
        } else if (arg == "-ofg") {
            generate_matrix_out = true;
            if (i + 1 >= argc) exit_with_error(std::cerr, INVALID_ARGUMENT_C, INVALID_ARGUMENT_MESSAGE);
            generate_output_path = argv[++i];
            std::cout << "-=-=Matrices will be generated to the following file: " << generate_output_path << "=-=-" << std::endl;
        } else exit_with_error(std::cerr, INVALID_ARGUMENT_C, INVALID_ARGUMENT_MESSAGE);
    }

    switch (matrix_input) {
        case 0: // cin
            std::cout << "Enter matrix one: " << std::endl;
            A = get_matrix_input(std::cin, m1, n1);
            std::cout << "Enter matrix two: " << std::endl;
            B = get_matrix_input(std::cin, m2, n2);
            if(n1 != m2) exit_with_error(std::cerr, WRONG_SIZE_C, WRONG_SIZE_MESSAGE);
            break;
        case 1: // file
            file_stream.open(matrix_path);
            if (!file_stream.is_open())
                exit_with_error(std::cerr, NOT_FOUND_C, NOT_FOUND_MESSAGE);
            A = get_matrix_input(file_stream, m1, n1);
            B = get_matrix_input(file_stream, m2, n2);
            file_stream.close();
            break;
        case 2: // generate
            m1 = get_random_size(MIN_MATRIX_SIZE, MAX_MATRIX_SIZE);
            n1 = get_random_size(MIN_MATRIX_SIZE, MAX_MATRIX_SIZE);
            m2 = n1;
            n2 = get_random_size(MIN_MATRIX_SIZE, MAX_MATRIX_SIZE);
            if (parallel) {
                A.resize(m1 * n1);
                B.resize(m2 * n2);
                generate_matrices_parallel(A, B);
            } else {
                generate_matrices(A, B, m1, n1, m2, n2);
            }
    }

    if (parallel) {
        C = multiply_matrices_parallel(A, B, m1, n1, m2, n2);
    } else {
        C = multiply_matrices(A, B, m1, n1, m2, n2);
    }

    if (generate_matrix_out) {
        std::ofstream outfile;
        outfile.open(generate_output_path);
        generate_print_matrix(outfile, A, m1, n1, "A");
        generate_print_matrix(outfile, B, m2, n2, "B");
        outfile.close();
    }

    if (print_matrix_out) {
        if (!matrix_output_path.empty()) {
            std::ofstream outfile;
            outfile.open(matrix_output_path);
            print_matrix(outfile, A, m1, n1, "A");
            print_matrix(outfile, B, m2, n2, "B");
            print_matrix(outfile, C, m1, n2, "C");
            outfile.close();

        } else {
            print_matrix(std::cout, A, m1, n1, "A");
            print_matrix(std::cout, B, m2, n2, "B");
            print_matrix(std::cout, C, m1, n2, "C");
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Taken " << to_ms(end - start).count() << " ms to finish.\n";
    return 0;

}
