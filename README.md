# Matrix-multiplication

## Description
This program is able to do matrix multiplication. It support generation of matrices and multithreading for the multiplication.

## Installation
Built on C++17 and CMake for compilation.

## Usage
The program accepts the following falgs: -p, -f <path>, -g, -o, -of <path>, -ofg <path>, --help

`--help`: Will write the flags and the usage of the program.

`-p`: Will enable multithreading. The program will use as many threads as there are cores in your hardware.

`-f <path>`: Will take the matrices from a file.

`-g`: Will generate matrices with sizes predefined in the source code.

`-o`: Will print the matrices to the std::cout.

`-of <path>`: Will print the matrices to a file.

`-ofg <path>`: Will print the matrices with to file in a suitable for input way.

The matrix input should follow a special format.
First line should contain the number of rows and columns(N and M), and after N lines with M numbers separated by spaces.
The input file should contain 2 matrices with the format specified above.

## Example output

`program -g -o`

```
-=-=Matrices with random sizes will be automatically generated=-=-
-=-=Matrices will be written to std::cout=-=-
Matrix A: (2, 2)
1.9631 9.86502 
3.81086 3.31216 

Matrix B: (2, 4)
5.7232 7.84351 5.98138 1.50097
3.63955 9.56576 4.856 4.82978

Matrix C: (2, 4)
47.1394 109.764 59.6465 50.5924
33.8651 61.5739 38.878 21.717

Taken 3 ms to finish.
```

## Benchmark
Benchmarks were made on randomly generated 2000x2000 matrices with values between 1 and 1000.
The PC used for benchmarking has 32 GB DDR5 Ram and a 12th Gen Intel i5-12600KF with a base speed
of 3.70 GHz with 10 cores and 16 logical processors.

10 tests have been made for single and multithreaded modes.

Single thread:
`5116 4970 4919 4914 4953 4930 4947 4894 4971 4885`. Mean: `4949`

Multi-thread:
`657 629 628 562 571 589 588 573 569 545`. Mean: `591`

For my machine, the speeed up is around 8 times.

